<div class="col-md-12">
            {{--  NAMA  --}}
            <div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
                {!! Form::label('nama', '* Nama Lengkap', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                    {!! Form::text('nama', null, ['class' => 'form-control','placeholder'=>'Nama Lengkap']) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                    <small class="text-danger">{{ $errors->first('nama') }}</small>
                </div>
            </div>

            {{--  JK  --}}
            <div class="form-group{{ $errors->has('jk') ? ' has-error' : '' }}">
                {!! Form::label('jk', '* Jenis Kelamin', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-4">
                    <label class="radio-inline">
                        @if ($alumni->jk == 'L')
                            {!! Form::radio('jk', 'L',true) !!} Laki - Laki
                        @else
                            {!! Form::radio('jk', 'L') !!} Laki - Laki
                        @endif
                    </label>
                </div>
                <div class="col-sm-4">
                    <label class="radio-inline">
                        @if ($alumni->jk == 'P')
                            {!! Form::radio('jk', 'P',true) !!} Perempuan
                        @else
                            {!! Form::radio('jk', 'P') !!} Perempuan
                        @endif
                    </label>
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                    <small class="text-danger">{{ $errors->first('jk') }}</small>
                </div>
            </div>

            {{--  TANGGAL LAHIR  --}}
            <div class="form-group{{ $errors->has('tanggal_lahir') ? ' has-error' : '' }}">
                {!! Form::label('tanggal_lahir', '* Tanggal Lahir', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                    {!! Form::date('tanggal_lahir', ( isset($alumni->alumni_meta->tanggal_lahir) ? $alumni->alumni_meta->tanggal_lahir : null ), ['class' => 'form-control']) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                    <small class="text-danger">{{ $errors->first('tanggal_lahir') }}</small>
                </div>
            </div>

            {{--  Alamat  --}}
            <div class="form-group{{ $errors->has('alamat') ? ' has-error' : '' }}">
                {!! Form::label('alamat', '* Alamat', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                    {!! Form::textarea('alamat', null, ['class' => 'form-control','placeholder'=>'Alamat','style' => 'width: 100%; height: 80px;']) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                <small class="text-danger">{{ $errors->first('alamat') }}</small>
                </div>
            </div>

            {{--  jurusan  --}}
            <div class="form-group{{ $errors->has('jurusan_id') ? ' has-error' : '' }}">
                {!! Form::label('jurusan_id', '* Jurusan', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                    {!! Form::select('jurusan_id',$jurusan ,null,array('class'=>'form-control')) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                  <small class="text-danger">{{ $errors->first('jurusan_id') }}</small>
                </div>
            </div>

            {{--  Tahun  --}}
            <div class="form-group{{ $errors->has('tahun_lulus') ? ' has-error' : '' }}">
                {!! Form::label('tahun_lulus', '* Tahun Lulus', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                    {!! Form::selectRange('tahun_lulus',1988,$tahun ,null,array('class'=>'col-8 form-control','title'=>'Tahun Lulus')) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                  <small class="text-danger">{{ $errors->first('tahun_lulus') }}</small>
                </div>
            </div>

            {{--  STATUS  --}}
            <div class="form-group{{ $errors->has('status_id') ? ' has-error' : '' }}">
                {!! Form::label('status_id', '* Status', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                    {!! Form::select('status_id',$status ,null,array('class'=>'form-control')) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                  <small class="text-danger">{{ $errors->first('status_id') }}</small>
                </div>
            </div>

            {{--  Alamat  --}}
            <div class="form-group{{ $errors->has('alamat_kantor_kampus') ? ' has-error' : '' }}">
                {!! Form::label('alamat_kantor_kampus', '* Alamat Kantor / Kampus', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                    {!! Form::textarea('alamat_kantor_kampus', null, ['class' => 'form-control','placeholder'=>'Alamat','style' => 'width: 100%; height: 80px;']) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                  <small class="text-danger">{{ $errors->first('alamat_kantor_kampus') }}</small>
                </div>
            </div>

            {{--  Email  --}}
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                {!! Form::label('email', '* Email', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                    {!! Form::text('email', null, ['class' => 'form-control','placeholder'=>'Email']) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                    <small class="text-danger">{{ $errors->first('email') }}</small>
                </div>
            </div>

            {{--  Telp  --}}
            <div class="form-group{{ $errors->has('telp') ? ' has-error' : '' }}">
                {!! Form::label('telp', '* No Telepon / HP', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                    {!! Form::text('telp', null, ['class' => 'form-control','placeholder'=>'No Telepon / HP']) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                    <small class="text-danger">{{ $errors->first('telp') }}</small>
                </div>
            </div>
</div>
<div class="btn-group pull-right">
    {!! Form::reset("Batal", ['class' => 'btn btn-default']) !!}
    {!! Form::submit('Simpan', ['class'=>'btn btn-primary']) !!}
</div>
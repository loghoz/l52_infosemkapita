<div class="col-sm-12">
    <div class="form-group{{ $errors->has('pembuka') ? ' has-error' : '' }}">
        {!! Form::label('pembuka', 'Pesan Pembuka', ['class'=>'control-label col-sm-4']) !!}
        <div class="col-sm-8">
            {!! Form::textarea('pembuka', null, ['class' => 'form-control','placeholder'=>'Isi Pembuka','style' => 'width: 100%; height: 80px;']) !!}
        </div>
        <div class="col-sm-4"></div>
        <div class="col-sm-8">
          <small class="text-danger">{{ $errors->first('pembuka') }}</small>
        </div>
    </div>

    <div class="form-group{{ $errors->has('penutup') ? ' has-error' : '' }}">
        {!! Form::label('penutup', 'Pesan Penutup', ['class'=>'control-label col-sm-4']) !!}
        <div class="col-sm-8">
            {!! Form::textarea('penutup', null, ['class' => 'form-control','placeholder'=>'Isi Penutup','style' => 'width: 100%; height: 80px;']) !!}
        </div>
        <div class="col-sm-4"></div>
        <div class="col-sm-8">
          <small class="text-danger">{{ $errors->first('penutup') }}</small>
        </div>
    </div>

    <div class="form-group{{ $errors->has('pengumuman') ? ' has-error' : '' }}">
        {!! Form::label('pengumuman', 'Pesan Pengumuman', ['class'=>'control-label col-sm-4']) !!}
        <div class="col-sm-8">
            {!! Form::textarea('pengumuman', null, ['class' => 'form-control','placeholder'=>'Isi Pengumuman','style' => 'width: 100%; height: 80px;']) !!}
        </div>
        <div class="col-sm-4"></div>
        <div class="col-sm-8">
        <small class="text-danger">{{ $errors->first('pengumuman') }}</small>
        </div>
    </div>

    <div class="form-group{{ $errors->has('info') ? ' has-error' : '' }}">
        {!! Form::label('info', 'Info', ['class'=>'control-label col-sm-4']) !!}
        <div class="col-sm-8">
          {!! Form::select('info',array('Aktif'=>'Aktif','Tidak Aktif'=>'Tidak Aktif') ,null,array('class'=>'form-control')) !!}
        </div>
        <div class="col-sm-4"></div>
        <div class="col-sm-8">
          <small class="text-danger">{{ $errors->first('info') }}</small>
        </div>
    </div>

    <div class="btn-group pull-right">
        {!! Form::reset("Batal", ['class' => 'btn btn-default']) !!}
        {!! Form::submit("Simpan", ['class' => 'btn btn-primary']) !!}
    </div>
</div>

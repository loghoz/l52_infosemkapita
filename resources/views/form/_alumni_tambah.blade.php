<div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }} row">
    <label class="col-4 col-form-label">Nama</label>
    {!! Form::text('nama', null, ['class' => 'col-8 form-control','placeholder'=>'Nama Lengkap','title'=>'Nama Lengkap']) !!}
    <label class="col-4 col-form-label"></label><small class="text-danger">{{ $errors->first('nama') }}</small>
</div>

<div class="form-group{{ $errors->has('jk') ? ' has-error' : '' }} row">
    <label class="col-4 col-form-label">Jenis Kelamin</label>
    {!! Form::select('jk',array('L'=>'Laki - Laki','P'=>'Perempuan') ,null,array('class'=>'col-8 form-control','title'=>'Jenis Kelamin')) !!}
    <label class="col-4 col-form-label"></label><small class="text-danger">{{ $errors->first('jk') }}</small>
</div>

<div class="form-group{{ $errors->has('tanggal_lahir') ? ' has-error' : '' }} row">
    <label class="col-4 col-form-label">Tanggal Lahir</label>
    {!! Form::date('tanggal_lahir', null, ['class' => 'col-8 form-control','title'=>'Tanggal Lahir']) !!}
    <label class="col-4 col-form-label"></label><small class="text-danger">{{ $errors->first('tanggal_lahir') }}</small>
</div>

<div class="form-group{{ $errors->has('alamat') ? ' has-error' : '' }} row">
    <label class="col-4 col-form-label">Alamat</label>
    {!! Form::textarea('alamat', null, ['class' => 'col-8 form-control','placeholder'=>'Alamat','style' => 'width: 100%; height: 80px;']) !!}
    <label class="col-4 col-form-label"></label><small class="text-danger">{{ $errors->first('alamat') }}</small>
</div>

<div class="form-group{{ $errors->has('jurusan_id') ? ' has-error' : '' }} row">
    <label class="col-4 col-form-label">Jurusan</label>
    {!! Form::select('jurusan_id',$jurusan ,null,array('class'=>'col-8 form-control','title'=>'Jurusan')) !!}
    <label class="col-4 col-form-label"></label><small class="text-danger">{{ $errors->first('jurusan_id') }}</small>
</div>

<div class="form-group{{ $errors->has('tahun_lulus') ? ' has-error' : '' }} row">
    <label class="col-4 col-form-label">Tahun Lulus</label>
    {!! Form::selectRange('tahun_lulus',1988,$tahun ,null,array('class'=>'col-8 form-control','title'=>'Tahun Lulus')) !!}
    <label class="col-4 col-form-label"></label><small class="text-danger">{{ $errors->first('tahun_lulus') }}</small>
</div>

<div class="form-group{{ $errors->has('status_id') ? ' has-error' : '' }} row">
    <label class="col-4 col-form-label">Status</label>
    {!! Form::select('status_id',$status ,null,array('class'=>'col-8 form-control','title'=>'Status')) !!}
    <label class="col-4 col-form-label"></label><small class="text-danger">{{ $errors->first('status_id') }}</small>
</div>

<div class="form-group{{ $errors->has('bekerja_kuliah') ? ' has-error' : '' }} row">
    <label class="col-4 col-form-label">Bekerja Di / Kuliah Di</label>
    {!! Form::textarea('bekerja_kuliah', null, ['class' => 'col-8 form-control','placeholder'=>'Bekerja Di / Kuliah Di','style' => 'width: 100%; height: 80px;']) !!}
    <label class="col-4 col-form-label"></label><small class="text-danger">{{ $errors->first('bekerja_kuliah') }}</small>
</div>

<div class="form-group{{ $errors->has('alamat_kantor_kampus') ? ' has-error' : '' }} row">
    <label class="col-4 col-form-label">Alamat Kantor / Kampus</label>
    {!! Form::textarea('alamat_kantor_kampus', null, ['class' => 'col-8 form-control','placeholder'=>'Alamat Kantor / Kampus','style' => 'width: 100%; height: 80px;']) !!}
    <label class="col-4 col-form-label"></label><small class="text-danger">{{ $errors->first('alamat_kantor_kampus') }}</small>
</div>

<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} row">
    <label class="col-4 col-form-label">E-Mail</label>
    {!! Form::text('email', null, ['class' => 'col-8 form-control','placeholder'=>'E-Mail','title'=>'Nama Lengkap']) !!}
    <label class="col-4 col-form-label"></label><small class="text-danger">{{ $errors->first('email') }}</small>
</div>

<div class="form-group{{ $errors->has('telp') ? ' has-error' : '' }} row">
    <label class="col-4 col-form-label">No Telepon / HP</label>
    {!! Form::text('telp', null, ['class' => 'col-8 form-control','placeholder'=>'Nomor Telepon / HP','title'=>'Nama Lengkap']) !!}
    <label class="col-4 col-form-label"></label><small class="text-danger">{{ $errors->first('telp') }}</small>
</div>

<div class="form-group" align="right">
    <div class="col-md-12">
        <button type="submit" class="btn btn-custom">
            Tambah Data
        </button>
    </div>
</div>
<div class="col-sm-12">

    <div class="form-group{{ $errors->has('no_ujian') ? ' has-error' : '' }}">
        {!! Form::label('no_ujian', 'Nomor Ujian', ['class'=>'control-label col-sm-4']) !!}
        <div class="col-sm-8">
          {!! Form::text('no_ujian', null, ['class' => 'form-control','placeholder'=>'Nomor Ujian']) !!}
        </div>
        <div class="col-sm-4"></div>
        <div class="col-sm-8">
          <small class="text-danger">{{ $errors->first('no_ujian') }}</small>
        </div>
    </div>

    <div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
        {!! Form::label('nama', 'Nama Siswa', ['class'=>'control-label col-sm-4']) !!}
        <div class="col-sm-8">
          {!! Form::text('nama', null, ['class' => 'form-control','placeholder'=>'Nama Siswa']) !!}
        </div>
        <div class="col-sm-4"></div>
        <div class="col-sm-8">
          <small class="text-danger">{{ $errors->first('nama') }}</small>
        </div>
    </div>

    <div class="form-group{{ $errors->has('nilai') ? ' has-error' : '' }}">
        {!! Form::label('nilai', 'Nilai', ['class'=>'control-label col-sm-4']) !!}
        <div class="col-sm-8">
          {!! Form::text('nilai', null, ['class' => 'form-control','placeholder'=>'Nilai']) !!}
        </div>
        <div class="col-sm-4"></div>
        <div class="col-sm-8">
          <small class="text-danger">{{ $errors->first('nilai') }}</small>
        </div>
    </div>

    <div class="btn-group pull-right">
        {!! Form::reset("Batal", ['class' => 'btn btn-default']) !!}
        {!! Form::submit("Simpan", ['class' => 'btn btn-primary']) !!}
    </div>
</div>

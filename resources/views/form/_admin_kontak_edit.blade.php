<div class="col-sm-12">
    <div class="form-group{{ $errors->has('meta_key') ? ' has-error' : '' }}">
        {!! Form::label('meta_key', 'Daftar', ['class'=>'control-label col-sm-2']) !!}
        <div class="col-sm-6">
          {!! Form::text('meta_key', null, ['class' => 'form-control','placeholder'=>'Daftar']) !!}
        </div>
        <div class="col-sm-2"></div>
        <div class="col-sm-6">
          <small class="text-danger">{{ $errors->first('meta_key') }}</small>
        </div>
    </div>

    <div class="form-group{{ $errors->has('meta_value') ? ' has-error' : '' }}">
        {!! Form::label('meta_value', 'Isi', ['class'=>'control-label col-sm-2']) !!}
        <div class="col-sm-6">
          {!! Form::text('meta_value', null, ['class' => 'form-control','placeholder'=>'Isi']) !!}
        </div>
        <div class="col-sm-2"></div>
        <div class="col-sm-6">
          <small class="text-danger">{{ $errors->first('meta_value') }}</small>
        </div>
    </div>

    <div class="btn-group pull-right">
        {!! Form::reset("Batal", ['class' => 'btn btn-default']) !!}
        {!! Form::submit("Simpan", ['class' => 'btn btn-primary']) !!}
    </div>
</div>

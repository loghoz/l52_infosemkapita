@extends('layouts.admin')

@section('content')

    <section class="content-header">
        <h1>
            Info
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Aktifasi Informasi</h3>
                    </div>
                    <div class="box-body">
                        {!! Form::model($info, ['route' => ['admin.aktifasi.update', $info],'method' =>'patch','class'=>'form-horizontal'])!!}
                            @include('form._admin_info_edit', ['model' => $info])
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@extends('layouts.admin')

@section('content')

    <section class="content-header">
        <h1>
            Kontak
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Kontak</h3>
                    </div>
                    <div class="box-body">
                        {!! Form::open(['route' => 'admin.kontak.store', 'class'=>'form-horizontal','files'=>true])!!}
                            @include('form._admin_kontak_tambah')
                        {!! Form::close() !!}
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Daftar</th>
                                <th>Isi</th>
                                <th colspan="2" align="center">Action</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($kontak as $item)
                                <tr>
                                    <td>{{ $item->meta_key }}</td>
                                    <td>{{ $item->meta_value }}</td>
                                    <td>
                                      <div class="col-md-12">
                                          <div class="col-md-6">
                                              <a href="{{ route('admin.kontak.edit', $item->id) }}" class="btn btn-primary">Ubah</a>
                                          </div>
                                          <div class="col-md-6">
                                              {{ Form::open(['route' => ['admin.kontak.destroy' , $item->id] ,'method' => 'DELETE']) }}
                                                  {{ Form::submit('Hapus', ['class' => 'btn btn-danger js-submit-confirm']) }}
                                              {{ Form::close() }}
                                          </div>
                                      </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

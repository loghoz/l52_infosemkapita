@extends('layouts.admin')

@section('content')

    <section class="content-header">
        <h1>
            Siswa
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">

                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Data Siswa</h3>
                    </div>
                    <div class="box-body">
                        {!! Form::model($siswa, ['route' => ['admin.infolulus.update', $siswa],'method' =>'patch','class'=>'form-horizontal'])!!}
                            @include('form._admin_siswa_edit', ['model' => $siswa])
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

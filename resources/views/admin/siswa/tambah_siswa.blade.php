@extends('layouts.admin')

@section('content')

    <section class="content-header">
        <h1>
            Siswa
        </h1>
    </section>
    
    <section class="content">
        <div class="row">
            <div class="col-md-8">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Tambah Siswa</h3>
                    </div>
                    <div class="box-body">
                        {!! Form::open(['route' => 'admin.infolulus.store', 'class'=>'form-horizontal','files'=>true])!!}
                            @include('form._admin_siswa_tambah')
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

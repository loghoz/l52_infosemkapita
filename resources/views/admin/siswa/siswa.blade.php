@extends('layouts.admin')

@section('content')

    <section class="content-header">
        <h1>
            Siswa
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">


                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Hapus Semuda Data Siswa</h3>
                    </div>

                    <div class="box-body" style="overflow-x:auto;">
                        {{ Form::open(['route' => ['admin.aktifasi.destroy' , '1'] ,'method' => 'DELETE']) }}
                            {{ Form::submit('Hapus', ['class' => 'btn btn-danger js-submit-confirm']) }}
                        {{ Form::close() }}
                    </div>
                </div>

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Data Siswa</h3>
                    </div>
                    <div class="box-body" style="overflow-x:auto;">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nomor Ujian</th>
                                    <th>Nama</th>
                                    <th>Nilai</th>
                                    <th>Info</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($siswa as $item)
                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ $item->no_ujian }}</td>
                                        <td>{{ $item->nama }}</td>
                                        <td>{{ $item->nilai }}</td>
                                        <td>{{ $item->info }}</td>
                                        <td><a href="{{ route('admin.infolulus.edit', $item->id) }}" class="btn btn-primary">Ubah</a></td>
                                        <td>
                                            {{ Form::open(['route' => ['admin.infolulus.destroy' , $item->id] ,'method' => 'DELETE']) }}
                                                {{ Form::submit('Hapus', ['class' => 'btn btn-danger js-submit-confirm']) }}
                                            {{ Form::close() }}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@extends('layouts.admin')

@section('content')

    <section class="content-header">
        <h1>
            Alumni
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">

                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Data Alumni</h3>
                    </div>
                    <div class="box-body">
                        {!! Form::model($alumni, ['route' => ['admin.alumni.update', $alumni],'method' =>'patch','class'=>'form-horizontal'])!!}
                            @include('form._admin_alumni_edit', ['model' => $alumni])
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

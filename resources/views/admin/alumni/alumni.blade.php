@extends('layouts.admin')

@section('content')

    <section class="content-header">
        <h1>
            Alumni
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">

                <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title">Data Alumni</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body" style="overflow-x:auto;">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Nama</th>
                            <th>JK</th>
                            <th>Tanggal Lahir</th>
                            <th>Alamat</th>
                            <th>Tahun Lulus</th>
                            <th>Jurusan</th>
                            <th>Status</th>
                            <th>Bekerja / Kuliah</th>
                            <th>Email / Telp</th>
                            <th>Ubah</th>
                            <th>Hapus</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($alumni as $item)
                            <tr>
                                <td>{{ $item->nama }}</td>
                                <td>{{ $item->jk }}</td>
                                <td>{{ $item->tanggal_lahir }}</td>
                                <td>{{ $item->alamat }}</td>
                                <td>{{ $item->tahun_lulus }}</td>
                                <td>{{ $item->jurusan->jurusan }}</td>
                                <td>{{ $item->status->status }}</td>
                                <td>
                                    {{ $item->bekerja_kuliah }}<br><br>
                                    Alamat : {{ $item->alamat_kantor_kampus }}
                                </td>
                                <td>{{ $item->email }}<br>{{ $item->telp }}</td>
                                <td><a href="{{ route('admin.alumni.edit', $item->id) }}" class="btn btn-primary">Ubah</a></td>
                                <td>
                                    {{ Form::open(['route' => ['admin.alumni.destroy' , $item->id] ,'method' => 'DELETE']) }}
                                        {{ Form::submit('Hapus', ['class' => 'btn btn-danger js-submit-confirm']) }}
                                    {{ Form::close() }}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
          </div>
            </div>
        </div>
    </section>
@endsection

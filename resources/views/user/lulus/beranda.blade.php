@extends('layouts.app')

@section('content')
<div class="container-fluid">

    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="btn-group pull-right">
                    <ol class="breadcrumb hide-phone p-0 m-0">
                        <li class="breadcrumb-item"><a href="/">Beranda</a></li>
                        <li class="breadcrumb-item active">infoLulus</li>
                        <li class="breadcrumb-item">Cek</li>
                    </ol>
                </div>
                <h4 class="page-title">infoLulus</h4>
            </div>
        </div>
    </div>
    <!-- end page title end breadcrumb -->


    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="card-box">
                <!-- <h4 class="header-title mb-4">Informasi</h4> -->

                <div class="row">
                    <div class="col-md-12" align="center">
                        <img src="itlabil/image/logo.png" width="200px" height="200px"><br><br>
                        <font size="4px">SILAHKAN MASUKKAN NOMOR UJIAN</font><br><br>
                    </div>
                    <div class="col-md-12" align="center">
                    {!! Form::open(['route' => 'lulus.store','class'=>'form-horizontal'])!!}
                        <div class="form-group{{ $errors->has('no_ujian') ? ' has-error' : '' }}">
                            {!! Form::text('no_ujian', null, ['class' => 'form-control','placeholder'=>'Nomor Ujian']) !!}
                            <small class="text-danger">{{ $errors->first('no_ujian') }}</small>
                        </div>

                        <div class="form-group" align="right">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-custom">
                                    Cari
                                </button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                    </div>
                </div>
                <!-- end row -->
            </div>
        </div>
        <div class="col-md-3"></div>
    </div>
    <!-- end row -->

</div> <!-- end container -->

@endsection

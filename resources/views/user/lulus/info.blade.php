@extends('layouts.app')

@section('content')
<div class="container-fluid">

    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="btn-group pull-right">
                    <ol class="breadcrumb hide-phone p-0 m-0">
                        <li class="breadcrumb-item"><a href="/">Beranda</a></li>
                        <li class="breadcrumb-item active">infoLulus</li>
                        <li class="breadcrumb-item">info</li>
                    </ol>
                </div>
                <h4 class="page-title">infoLulus</h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card-box">

                <div class="row">
                    <div class="col-md-12" align="center">
                        <img src="itlabil/image/logo.png" width="200px" height="200px">
                        <br><br><br><br>
                        <table>
                            <tr>
                                <td width="90px">Nomor Ujian</td>
                                <td> : {{$siswa->no_ujian}}</td>
                            </tr>
                            <tr>
                                <td>Nama</td>
                                <td> : {{$siswa->nama}}</td>
                            </tr>
                            <tr>
                                <td colspan="2" align="justify"><br>
                                {{$info->pembuka}}
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2"><br>
                                    <center><b><font size="5px">"{{$siswa->info}}"
                                    @if($siswa->nilai=="")
                                    @else
                                    <br>Dengan Jumlah Nilai Ujian Nasional = {{$siswa->nilai}}</font></b>
                                    @endif
                                    </center>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" align="justify"><br>
                                {{$info->penutup}}
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2"><br>
                                    <i>*Info : {!!$info->pengumuman!!}</i>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" align="right"><br>
                                    <a href="/ceklulus" class="btn btn-custom">Kembali</a>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection

@extends('layouts.app')

@section('content')
<div class="container-fluid">

    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="btn-group pull-right">
                    <ol class="breadcrumb hide-phone p-0 m-0">
                        <li class="breadcrumb-item"><a href="/">Beranda</a></li>
                        <li class="breadcrumb-item active">Alumni</li>
                        <li class="breadcrumb-item">Tambah Data Alumni</li>
                    </ol>
                </div>
                <h4 class="page-title">Alumni</h4>
            </div>
        </div>
    </div>
    <!-- end page title end breadcrumb -->


    <div class="row">
        <div class="col-md-12">
            <div class="card-box">
                <!-- <h4 class="header-title mb-4">Informasi</h4> -->

                <div class="row">
                    <div class="col-md-12" align="center">
                        <font size="4px">Isi Biodata Alumni</font><br><br>
                    </div>
                    <div class="col-md-12">
                        {!! Form::open(['route' => 'alumni.store','class'=>'form-horizontal'])!!}
                            @include('form._alumni_tambah')
                        {!! Form::close() !!}
                    </div>
                </div>
                <!-- end row -->
            </div>
        </div>
    </div>
    <!-- end row -->

</div> <!-- end container -->

@endsection

@extends('layouts.app')

@section('content')
<div class="container-fluid">

    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="btn-group pull-right">
                    <ol class="breadcrumb hide-phone p-0 m-0">
                        <li class="breadcrumb-item"><a href="/">Beranda</a></li>
                        <li class="breadcrumb-item active">Alumni</li>
                        <li class="breadcrumb-item">Data Alumni</li>
                    </ol>
                </div>
                <h4 class="page-title">Alumni</h4>
            </div>
        </div>
    </div>
    <!-- end page title end breadcrumb -->


    <div class="row">
        <div class="col-md-12">
            <div class="card-box">

                <div class="row">
                    <div class="col-md-12">
                        <font size="4px">Data Alumni SMK Pelita Pesawaran</font><br><br>
                    </div>
                    <div class="col-md-12">
                        <div class="card-box table-responsive">
                            <table id="responsive-datatable" class="table table-bordered table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Nama</th>
                                        <th>Jenis Kelamin</th>
                                        <th>Tahun Lulus</th>
                                        <th>Jurusan</th>
                                        <th>Status</th>
                                        <th>Bekerja Di / Kuliah Di</th>
                                        <th>Alamat Kantor / Kampus</th>
                                    </tr>
                                </thead>


                                <tbody>
                                    @foreach($alumni as $item)
                                        <tr>
                                            <td>{{ $item->nama }}</td>
                                            @if($item->jk=='L')
                                                <td>Laki - Laki</td>
                                            @else
                                                <td>Perempuan</td>
                                            @endif
                                            <td>{{ $item->tahun_lulus}}</td>
                                            <td>{{ $item->jurusan->jurusan }}</td>
                                            <td>{{ $item->status->status }}</td>
                                            <td>{{ $item->bekerja_kuliah }}</td>
                                            <td>{{ $item->alamat_kantor_kampus }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-12">
                    <i>*Jika ada perubahan data, silahkan hubungi <a href="/kontak">Admin SMK Pelita Pesawaran</a>.</i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end row -->

</div> <!-- end container -->

@endsection

@extends('layouts.app')

@section('content')
<div class="container-fluid">

    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="btn-group pull-right">
                    <ol class="breadcrumb hide-phone p-0 m-0">
                        <li class="breadcrumb-item"><a href="/">Beranda</a></li>
                        <li class="breadcrumb-item active">Kontak</li>
                    </ol>
                </div>
                <h4 class="page-title">Kontak</h4>
            </div>
        </div>
    </div>
    <!-- end page title end breadcrumb -->


    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <!-- <h4 class="header-title mb-4">Informasi</h4> -->

                <div class="row">
                    <div class="col-md-12" align="center">
                        <img src="itlabil/image/logo.png" width="150px" height="150px"><br><br>
                    </div>
                    <div class="col-md-12">
                        <table class="table table-borderless mb-0">
                            <tbody>
                                @foreach($kontak as $item)
                                    <tr>
                                        <td>{{ $item->meta_key }}</td>
                                        @if('http'==str_limit($item->meta_value, $limit = 4, $end=''))
                                            <td>: <a href="{{ $item->meta_value }}">{{ $item->meta_value }}</a></td>
                                        @else
                                            <td>: {{ $item->meta_value }}</td>
                                        @endif
                                    </tr>
                                @endforeach
                            </tbody>
                            
                        </table>
                    </div>
                </div>
                <!-- end row -->
            </div>
        </div>
    </div>
    <!-- end row -->

</div> <!-- end container -->
@endsection

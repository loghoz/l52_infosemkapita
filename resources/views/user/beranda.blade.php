@extends('layouts.app')

@section('content')
<div class="container-fluid">

    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="btn-group pull-right">
                    <ol class="breadcrumb hide-phone p-0 m-0">
                        <li class="breadcrumb-item"><a href="#">Beranda</a></li>
                        <!-- <li class="breadcrumb-item active">Dashboard</li> -->
                    </ol>
                </div>
                <h4 class="page-title">Beranda</h4>
            </div>
        </div>
    </div>
    <!-- end page title end breadcrumb -->


    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <!-- <h4 class="header-title mb-4">Informasi</h4> -->

                <div class="row">
                    <div class="col-md-12" align="center">
                        <img src="itlabil/image/logo.png" width="200px" height="200px"><br><br>
                        <font size="4px">WEBSITE INFORMASI</font><br>
                        <font size="5px"><b>SMK PELITA PESAWARAN</b></font><br>
                        <font size="4px">PROVINSI LAMPUNG</font><br><br>
                    </div>
                    <div class="col-md-12" align="center">
                        <img src="itlabil/image/4.png" width="60px">
                        <img src="itlabil/image/3.png" width="60px">
                        <img src="itlabil/image/5.png" width="60px">
                        <img src="itlabil/image/2.png" width="60px">
                        <img src="itlabil/image/6.png" width="60px">
                        <img src="itlabil/image/7.png" width="60px">
                    </div>
                </div>
                <!-- end row -->
            </div>
        </div>
    </div>
    <!-- end row -->

</div> <!-- end container -->
@endsection

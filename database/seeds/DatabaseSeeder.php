<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(StatusSeeder::class);
        $this->call(JurusanSeeder::class);
        $this->call(KontakSeeder::class);
        $this->call(InfoSeeder::class);
        $this->call(SiswaSeeder::class);
        // $this->call(AlumniSeeder::class);
    }
}

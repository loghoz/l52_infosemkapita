<?php

use Illuminate\Database\Seeder;
use App\Kontak;
class KontakSeeder extends Seeder
{
    public function run()
    {
        Kontak::create([
            'meta_key'      => 'Alamat',
            'meta_value'    => 'Jl. Raya Penengahan No. 04 Gedongtataan',
        ]);
        Kontak::create([
            'meta_key'      => 'No Telp. Sekolah',
            'meta_value'    => '0721 94092',
        ]);
        Kontak::create([
            'meta_key'      => 'Email',
            'meta_value'    => 'smkpelitasmk@yahoo.com',
        ]);
        Kontak::create([
            'meta_key'      => 'Facebook',
            'meta_value'    => 'https://www.facebook.com/smkpelita.pesawaran',
        ]);
        Kontak::create([
            'meta_key'      => 'Instagram',
            'meta_value'    => '-',
        ]);
        Kontak::create([
            'meta_key'      => 'Group Whatsapp',
            'meta_value'    => '-',
        ]);
        Kontak::create([
            'meta_key'      => 'Group Telegram',
            'meta_value'    => '-',
        ]);
    }
}

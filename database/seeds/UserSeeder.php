<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //sample admin
        App\User::create([
            'no_induk' => '9999999999',
            'email' => 'admin@smkpelitapesawaran.sch.id',
            'username' => 'admin',
            'password' => bcrypt('1sampai9'),
            'api_token' => bcrypt('admin@smkpelitapesawaran.sch.id'),
            'role' => 'admin'
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;

use App\Siswa;
class SiswaSeeder extends Seeder
{

    public function run()
    {
        Siswa::create([
            'no_induk'          => '1111',
            'no_ujian'          => 'K1211010200000',
            'nama'              => 'ERWIN DIANTO',
            'info'              => 'Lulus'
        ]);
    }
}

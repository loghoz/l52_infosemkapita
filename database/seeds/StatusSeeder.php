<?php

use Illuminate\Database\Seeder;
use App\Status;

class StatusSeeder extends Seeder
{
    public function run()
    {
        Status::create([
            'status' => '-'
        ]);
        Status::create([
            'status' => 'Tidak Bekerja'
        ]);
        Status::create([
            'status' => 'Kuliah'
        ]);
        Status::create([
            'status' => 'Nelayan'
        ]);
        Status::create([
            'status' => 'Petani'
        ]);
        Status::create([
            'status' => 'Peternak'
        ]);
        Status::create([
            'status' => 'PNS'
        ]);
        Status::create([
            'status' => 'TNI'
        ]);
        Status::create([
            'status' => 'Polri'
        ]);
        Status::create([
            'status' => 'Karyawan Swasta'
        ]);
        Status::create([
            'status' => 'Pedagang Kecil'
        ]);
        Status::create([
            'status' => 'Pedagang Besar'
        ]);
        Status::create([
            'status' => 'Wiraswasta'
        ]);
        Status::create([
            'status' => 'Wirausaha'
        ]);
        Status::create([
            'status' => 'Buruh'
        ]);
        Status::create([
            'status' => 'Pensiunan'
        ]);
        Status::create([
            'status' => 'Lainnya'
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;

use App\Info;
class InfoSeeder extends Seeder
{

    public function run()
    {
        Info::create([
            'info'          => 'Aktif',
            'pembuka'       => 'Telah mengikuti Ujian Nasional Tahun Ajaran 2018 / 2019, berdasarkan hasil penilaian dan kriteria dari sekolah bahwa nama yang tertera diatas di nyatakan :',
            'penutup'       => '',
            'pengumuman'    => 'Sidik jari & Pengambilan Ijazah menggunakan seragam putih abu - abu.!!',
        ]);
    }
}

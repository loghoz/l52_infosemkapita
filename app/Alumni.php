<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alumni extends Model
{
    protected $table = 'alumnis';
    protected $with = ['jurusan'];
    protected $fillable = [
        'status_id','jurusan_id','nama','jk','tanggal_lahir',
        'tahun_lulus','bekerja_kuliah','alamat_kantor_kampus','alamat','email','telp'
    ];

    // relasi ke jurusan
    public function jurusan()
    {
        return $this->belongsTo('App\Jurusan', 'jurusan_id');
    }

    // relasi ke status
    public function status()
    {
        return $this->belongsTo('App\Status', 'status_id');
    }

}

<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AlumniUserRequest extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
            {
                return [
                    'nama'  => 'required',
                    'tanggal_lahir' => 'required',
                    'alamat' => 'required',
                    'email' => 'required',
                    'telp' => 'required'
                ];
            }

            case 'PUT':
            case 'PATCH':
            {
                return [
                    'nama'  => 'required',
                    'tanggal_lahir' => 'required',
                    'alamat' => 'required',
                    'email' => 'required',
                    'telp' => 'required'
                ];
            }

            default:break;
        }
    }

    public function messages()
    {
        return [

            'nama.required' => 'Tidak Boleh Kosong',
            'tanggal_lahir.required' => 'Tidak boleh kosong',
            'alamat.required' => 'Tidak boleh kosong',
            'email.required' => 'Tidak boleh kosong',
            'telp.required' => 'Tidak boleh kosong'
        ];
    }
}

<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SiswaRequest extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
            {
                return [
                    'no_ujian' => 'required',
                    'nama' => 'required'
                ];
            }

            case 'PUT':
            case 'PATCH':
            {
                return [
                    'no_ujian' => 'required',
                    'nama' => 'required'
                ];
            }

            default:break;
        }
    }

    public function messages()
    {
        return [

            'no_ujian.required' => 'Tidak boleh kosong',
            'nama.required' => 'Tidak boleh kosong'
        ];
    }
}

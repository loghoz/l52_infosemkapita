<?php

namespace App\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Jurusan;
use App\Status;
use App\Alumni;

class AlumniController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request)
    {
        $q = $request->get('q');

        $page = $request->get('page');

        $no = 1;

        if($page>1){
            $no = $page * 10 - 9;
        }else{
            $no=1;
        }
        
        $alumni = Alumni::get()->all();

        return view('admin.alumni.alumni', compact('alumni','no'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $tahun = date('Y');
        $jurusan = Jurusan::lists('jurusan', 'id');
        $status = Status::lists('status', 'id');

        $alumni = Alumni::findOrFail($id);

        return view('admin.alumni.edit_alumni', compact('alumni','tahun','jurusan','status'));
    }

    public function update(Request $request, $id)
    {
        $alumni = Alumni::findOrFail($id);

        $alumni->update($request->all());
        $notification = array(
            'message' => 'Data Alumni dengan nama '.$alumni->nama.' berhasil diubah.',
            'alert-type' => 'success'
        );
        return redirect()->route('admin.alumni.index')->with($notification);
    }

    public function destroy($id)
    {
        $alumni = Alumni::findOrFail($id);
        $notification = array(
            'message' => 'Data Alumni dengan nama '.$alumni->nama.' berhasil dihapus.',
            'alert-type' => 'error'
        );
        Alumni::find($id)->delete();

        return redirect()->route('admin.alumni.index')->with($notification);
    }
}

<?php

namespace App\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Info;
use App\Siswa;

class InfoController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $id=1;
        $info = Info::findOrFail($id);
        return view('admin.info.info',compact('info'));
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        $info = Info::findOrFail($id);

        $info->update($request->all());
        $notification = array(
            'message' => 'Aktifasi pengecekan kelulusan berhasil di rubah ke '.$info->info.'.',
            'alert-type' => 'success'
        );
        return redirect()->route('admin.aktifasi.index')->with($notification);
    }

    public function destroy($id)
    {
        $notification = array(
            'message' => 'Data Semua Siswa berhasil dihapus.',
            'alert-type' => 'error'
        );
        Siswa::where('id', 'like','%%')->delete();
        return redirect()->route('admin.infolulus.index')->with($notification);
    }
}

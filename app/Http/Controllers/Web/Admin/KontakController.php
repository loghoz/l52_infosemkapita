<?php

namespace App\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Kontak;

class KontakController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $kontak = Kontak::get()->all();
        return view('admin.kontak.kontak',compact('kontak'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $kontak = $request->all();
        Kontak::create($kontak);

        $notification = array(
            'message' => 'Data Kontak berhasil ditambah.',
            'alert-type' => 'success'
        );

        return redirect()->route('admin.kontak.index')->with($notification);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $kontak = Kontak::findOrFail($id);

        return view('admin.kontak.edit_kontak', compact('kontak'));
    }

    public function update(Request $request, $id)
    {
        $kontak = Kontak::findOrFail($id);

        $kontak->update($request->all());
        $notification = array(
            'message' => 'Data Kontak berhasil diubah.',
            'alert-type' => 'success'
        );
        return redirect()->route('admin.kontak.index')->with($notification);
    }

    public function destroy($id)
    {
        $kontak = Kontak::findOrFail($id);
        $notification = array(
            'message' => 'Data Kontak berhasil dihapus.',
            'alert-type' => 'error'
        );
        Kontak::find($id)->delete();
        return redirect()->route('admin.kontak.index')->with($notification);
    }
}

<?php

namespace App\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\SiswaRequest;
use App\Http\Controllers\Controller;

use App\Siswa;

class SiswaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $no = 1;

        $siswa = Siswa::orderBy('nama','asc')->get()->all();

        return view('admin.siswa.siswa', compact('siswa','no'));
    }

    public function create()
    {
        return view('admin.siswa.tambah_siswa');
    }

    public function store(SiswaRequest $request)
    {
        // SIMPAN DATA SISWA META
        $siswa = $request->only('nama','no_ujian','nilai');
        $siswa['info'] = 'Lulus';

        Siswa::create($siswa);

        $notification = array(
            'message' => 'Siswa berhasil ditambah.',
            'alert-type' => 'success'
        );
        return redirect()->route('admin.infolulus.create')->with($notification);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $siswa = Siswa::findOrFail($id);

        return view('admin.siswa.edit_siswa', compact('siswa'));
    }

    public function update(SiswaRequest $request, $id)
    {
        $siswa = Siswa::findOrFail($id);

        $siswa->update($request->all());
        $notification = array(
            'message' => 'Data Siswa berhasil diubah.',
            'alert-type' => 'success'
        );
        return redirect()->route('admin.infolulus.index')->with($notification);
    }

    public function destroy($id)
    {
        $siswa = Siswa::findOrFail($id);
        $notification = array(
            'message' => 'Data Siswa berhasil dihapus.',
            'alert-type' => 'error'
        );
        Siswa::find($id)->delete();
        return redirect()->route('admin.infolulus.index')->with($notification);
    }
}

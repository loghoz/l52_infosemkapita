<?php

namespace App\Http\Controllers\Web\User;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\AlumniUserRequest;;
use App\Http\Controllers\Controller;

use App\Jurusan;
use App\Status;
use App\Alumni;

class AlumniController extends Controller
{

    public function index()
    {
        $alumni = Alumni::orderBy('id','desc')->get()->all();

        return view('user.alumni.alumni', compact('alumni'));
    }

    public function create()
    {
        $tahun = date('Y');
        $jurusan = Jurusan::lists('jurusan', 'id');
        $status = Status::lists('status', 'id');

        return view('user.alumni.tambahdata', compact('jurusan','status','tahun'));
    }


    public function store(AlumniUserRequest $request)
    {
        Alumni::create($request->all());
        
        $notification = array(
            'message' => 'Data Alumni dengan nama '.$request->nama.' telah berhasil ditambah.',
            'alert-type' => 'success'
        );

        return redirect()->route('alumni.index')->with($notification);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers\Web\User;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Siswa;
use App\Info;

class InfoController extends Controller
{
    public function index(Request $request)
    {
        $no_ujian = $request->cookie('no_ujian');
        $siswa = Siswa::where('no_ujian',$no_ujian)->first();
        $info = Info::get()->first();

        return view('user.lulus.info',compact('siswa','info'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.lulus.beranda');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $info = Info::where('id',1)->first();

        if ($info->info=='Aktif') {
            $no_ujian = $request->get('no_ujian');
            $siswa = Siswa::where('no_ujian','LIKE',$no_ujian)->first();

            if (!empty($siswa)) {
                $noujian = cookie()->forever('no_ujian', $siswa->no_ujian);

                return redirect()->route('lulus.index')
                        ->withCookie($noujian);
            } else {
                $notification = array(
                    'message' => 'Nomor Ujian Salah, Mohon di Cek Kembali.',
                    'alert-type' => 'warning'
                );

                return redirect('ceklulus')->with($notification);
            }
        } else {
            $notification = array(
                'message' => 'Pengecekan Info Lulus belum di Buka.',
                'alert-type' => 'error'
            );

            return redirect('ceklulus')->with($notification);
        }
        

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::resource('/', 'Web\User\BerandaController');
Route::resource('/lulus', 'Web\User\InfoController');
Route::resource('/ceklulus', 'Web\User\InfoController@create');
Route::resource('/alumni', 'Web\User\AlumniController');
Route::resource('/tambah_alumni', 'Web\User\AlumniController@create');
Route::resource('/kontak', 'Web\User\KontakController');

Route::auth();

//ADMIN
Route::resource('/admin/beranda', 'Web\Admin\BerandaController');
Route::resource('/admin/infolulus', 'Web\Admin\SiswaController');
Route::resource('/admin/alumni', 'Web\Admin\AlumniController');
Route::resource('/admin/kontak', 'Web\Admin\KontakController');
Route::resource('/admin/aktifasi', 'Web\Admin\InfoController');

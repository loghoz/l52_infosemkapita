<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jurusan extends Model
{
    protected $table = 'jurusans';

    protected $fillable = [
        'kode_jurusan','jurusan','bidang_keahlian',
    ];

    // relasi one to many ke alumni
    public function alumni()
    {
        return $this->hasMany('App\Alumni');
    }
}
